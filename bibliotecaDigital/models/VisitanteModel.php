<?php
include_once '../models/ConexaoModel.php';

class VisitanteModel{
	
 	private $nome;
	private $telefone;
	private $endereco;
	private $matricula;
	private $senha;
	
	public function getNome() {
		return $this->nome;
	}
	private function setNome($nome) {
		$this->nome = $nome;
		return $this;
	}
	public function getTelefone() {
		return $this->telefone;
	}
	private function setTelefone($telefone) {
		$this->telefone = $telefone;
		return $this;
	}
	public function getEndereco() {
		return $this->endereco;
	}
	private function setEndereco($endereco) {
		$this->endereco = $endereco;
		return $this;
	}
	public function getMatricula() {
		return $this->matricula;
	}
	private function setMatricula($matricula) {
		$this->matricula = $matricula;
		return $this;
	}
	public function getSenha() {
		return $this->senha;
	}
	private function setSenha($senha) {
		$this->senha = $senha;
		return $this;
	}
	public function __construct($nome=null,$telefone=null,$endereco=null,$matricula=null,$senha=null){
		$this->nome = $nome;
		$this->telefone = $telefone;
		$this->endereco = $endereco;
		$this->matricula = $matricula;
		$this->senha = $senha;
	}
	public function salvar(){
		$conn = new ConexaoModel();
		$sql = "INSERT INTO `biblioteca`.`visitantes` (`nome`, `telefone`, `endereco`, `matricula`, `senha`) 
				VALUES ('$this->nome', $this->telefone, '$this->endereco', '$this->matricula', $this->senha)";
		
		$reultado = mysql_query($sql,$conn->conectar());
		return $reultado;
	;
	}
	public function listaVisitantes(){
		$conn = new ConexaoModel();
		$sql = "SELECT	 * FROM biblioteca.visitantes";
		
		$resultado = mysql_query($sql,$conn->conectar());
		while($row = mysql_fetch_array($resultado)){
			$visitantes[] = $row;
		}
		return $visitantes;
	}
	public function excluir($id){
		$conn = new ConexaoModel();
		$sql = "DELETE FROM  `biblioteca`.`visitantes` WHERE `id`= $id";
		$resultado = mysql_query($sql,$conn->conectar());
		
		return $resultado;
	}
	public function getVisitante($id){
		$conn = new ConexaoModel();
		$sql = "SELECT id,nome,telefone,endereco,matricula,senha,situacao_id FROM `biblioteca`. `visitantes` WHERE id =$id";
		$resultado = mysql_query($sql,$conn->conectar());
		$visitante= mysql_fetch_assoc($resultado);		
		
		return $visitante;
	}
	public function buscar($id){
		$conn = new ConexaoModel();
		$sql = "SELECT * FROM `biblioteca`,`visitantes`WHERE id = $id";
		$resultado = mysql_query($sql,$conn->conectar());
		
		$resultadoBusca =mysql_fetch_object($resultado);
		return $resultadoBusca;
	}
	public function editaVisitante($id){
		$conn = new ConexaoModel();
		$sql = "UPDATE `biblioteca`.`visitantes` 
				SET `nome`='$this->nome', `telefone`=$this->telefone, `endereco`='$this->endereco', `matricula`='$this->matricula', `senha`=$this->senha WHERE `id`='$id'";
		$resultado  = mysql_query($sql,$conn->conectar());
		return $resultado;
	}
	
	
}
