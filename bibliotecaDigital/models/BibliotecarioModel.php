<?php 
include_once 'ConexaoModel.php';

class BibliotecarioModel{
	//Declaração de variaveis;
	private $nome;
	private $telefone;
	private $endereco;
	private $senha;
	
	
	public function __construct($nome=null,$telefone=null,$endereco=null,$senha=null){
		$this->nome = $nome;
		$this->telefone= $telefone;
		$this->endereco = $endereco;
		$this->senha = $senha;
		
	}
	public function getNome() {
		return $this->nome;
	}
	private function setNome($nome) {
		$this->nome = $nome;
		return $this;
	}
	public function getTelefone() {
		return $this->telefone;
	}
	private function setTelefone($telefone) {
		$this->telefone = $telefone;
		return $this;
	}
	public function getEndereco() {
		return $this->endereco;
	}
	private function setEndereco($endereco) {
		$this->endereco = $endereco;
		return $this;
	}
	public function getSenha() {
		return $this->senha;
	}
	private function setSenha($senha) {
		$this->senha = $senha;
		return $this;
	}
	//Função para salvar bibliotecarios no banco de dados;
	public function salvar(){
		$conn = new ConexaoModel();
	
		$sql = "INSERT INTO `biblioteca`.`bibliotecarios` (`nome`,`telefone`,`endereco`, `senha`)
		VALUES ('$this->nome','$this->telefone ','$this->endereco ', '$this->senha ')";
		$resultado = mysql_query($sql,$conn->conectar());
		return $resultado;
	
	}
	
	public function listaBibliotecarios(){
		$conn = new ConexaoModel();
	
		$sql = "SELECT id,nome,telefone,endereco FROM bibliotecarios";
		$result = mysql_query($sql, $conn->conectar());
	
		while($row = mysql_fetch_array($result)){
			$bibliotecarios[] = $row;
		}
	
		return $bibliotecarios;
	}
	public function getBibliotecario($id){
	
		$conn = new ConexaoModel();
	
		$sql= "SELECT id,nome,telefone,endereco,senha FROM `biblioteca`.`bibliotecarios` WHERE `id`='$id'";
		
		$result = mysql_query($sql, $conn->conectar());
		$bibliotecario = mysql_fetch_assoc($result);
	
		return $bibliotecario;
	}
	
	//Função para excluir bibliotecarioss do banco de dados;
	public function excluir($id){
		$conn = new ConexaoModel();
	
		$sql = "DELETE FROM biblioteca.bibliotecarios WHERE `id` = '$id'";
		$resultado = mysql_query($sql,$conn->conectar());
		return $resultado;
	}
	public function buscar($id){
		$conn = new ConexaoModel();
		$sql	="SELECT * FROM biblioteca.bibliotecarios WHERE id=$id ";
		$resultado	= mysql_query($sql,$conn->conectar());
	
		$resultadoBusca= mysql_fetch_object($resultado);
		return $resultadoBusca;
	}
	public function editaBibliotecario($id){
		$conn = new ConexaoModel();
		
		$sql ="UPDATE `biblioteca`.`bibliotecarios` SET `nome`='$this->nome', `telefone`='$this->telefone', `endereco`='$this->endereco', `senha`='$this->senha' WHERE `id`='$id'";
		
		$resultado = mysql_query($sql,$conn->conectar());
		return $resultado;
	}
	
	
}




?>