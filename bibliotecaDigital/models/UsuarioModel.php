<?php 
include_once 'ConexaoModel.php';

class UsuarioModel{
	//Declaração de variaveis;
	private $nome;
	private $telefone;
	private $endereco;
	private $perfil;
	private $senha;
	
	public function getNome() {
		return $this->nome;
	}
	private function setNome($nome) {
		$this->nome = $nome;
		
	}
	public function getTelefone() {
		return $this->telefone;
	}
	private function setTelefone($telefone) {
		$this->telefone = $telefone;
		
	}
	public function getEndereco() {
		return $this->endereco;
	}
	private function setEndereco($endereco) {
		$this->endereco = $endereco;
		
	}
	public function getPerfil() {
		return $this->perfil;
	}
	private function setPerfil($perfil) {
		$this->perfil = $perfil;
		
	}
	public function getSenha() {
		return $this->senha;
	}
	private function setSenha($senha) {
		$this->senha = $senha;
		
		
	}
	public function __construct($nome=null,$telefone=null,$endereco=null,$senha=null,$perfil=NULL){
		$this->nome = $nome;
		$this->telefone= $telefone;
		$this->endereco = $endereco;
		$this->senha = $senha;
		$this->perfil = $perfil;
	}
	//Função para salvar usuarios no banco de dados;
	public function salvar(){
		$conn = new ConexaoModel();
	
		$sql = "INSERT INTO `biblioteca`.`usuarios` (`nome`, `telefone`, `endereco`, `senha`, `perfil_id`)
				VALUES ('$this->nome', $this->telefone, '$this->endereco', '$this->senha', '$this->perfil')";
		
		$resultado = mysql_query($sql,$conn->conectar());
		
		return $resultado;
	
	}
	
	public function listaUsuarios(){
		$conn = new ConexaoModel();
	
		$sql = "SELECT usuarios.id,usuarios.nome,usuarios.telefone,usuarios.endereco,perfil.descricao 
				FROM usuarios AS usuarios
				INNER JOIN perfil AS perfil
    				ON perfil.id = usuarios.perfil_id ";
		
		$resultado = mysql_query($sql, $conn->conectar());
		
		while($row = mysql_fetch_array($resultado)){
			$usuarios[] = $row;
		}
		
		return $usuarios;
	}
	public function getUsuarios($id){
	
		$conn = new ConexaoModel();
	
		$sql= "SELECT id,nome,telefone,endereco,senha FROM `biblioteca`.`usuarios` WHERE `id`='$id'";
		
		$result = mysql_query($sql, $conn->conectar());
		$usuario = mysql_fetch_assoc($result);
	
		return $usuario;
	}
	public function getPerfis(){
		$conn = new ConexaoModel();
		
		$query = "SELECT * FROM `biblioteca`.`perfil`";
		$result = mysql_query($query, $conn->conectar());
		
		$perfis= array();
		while($row = mysql_fetch_array($result)){
			$perfis[] = $row;
		}
		
		return $perfis;
	}
	
	public function excluir($id){
		$conn = new ConexaoModel();
	
		$sql = "DELETE FROM biblioteca.usuarios WHERE `id` = '$id'";
		$resultado = mysql_query($sql,$conn->conectar());
		return $resultado;
	}
	public function buscar($id){
		$conn = new ConexaoModel();
		$sql	="SELECT * FROM biblioteca.usuarios WHERE id=$id ";
		$resultado	= mysql_query($sql,$conn->conectar());
	
		$resultadoBusca= mysql_fetch_object($resultado);
		return $resultadoBusca;
	}
	public function editaUsuarios($id){
		$conn = new ConexaoModel();
		
		$sql ="UPDATE `biblioteca`.`usuarios` SET `nome`='$this->nome', `telefone`='$this->telefone', `endereco`='$this->endereco', `senha`='$this->senha',`perfil_id`='$this->perfil' WHERE `id`='$id'";
		
		$resultado = mysql_query($sql,$conn->conectar());
		return $resultado;
	}
	
}
?>