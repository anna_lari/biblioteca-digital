<?php
ini_set ( 'display_errors', 1 );
ini_set ( 'log_errors', 1 );
ini_set ( 'error_log', dirname ( '_FILE_' ) . '/error_log.txt' );
error_reporting ( E_ALL );

include 'ConexaoModel.php';

class LivroModel {
	
	private $titulo;
	private $autor;
	private $genero;
	
	public function getTitulo() {
		return $this->titulo;
	}
	private function setTitulo($titulo) {
		$this->titulo = $titulo;
		return $this;
	}
	public function getAutor() {
		return $this->autor;
	}
	private function setAutor($autor) {
		$this->autor = $autor;
		return $this;
	}
	private function setGenero($genero) {
		$this->genero = $genero;
		return $this;
	}
	
	//métodos públicos para LivroController poder ter acesso aos métodos 
	public  function __construct($titulo=null,$autor=null,$genero=null){
		
		$this->titulo = $titulo;
		$this->autor  = $autor;
		$this->genero = $genero;
	}
	
	public function cadastraLivro(){
		$conn = new ConexaoModel();
		
		$sql = "INSERT INTO `biblioteca`.`livros` (`titulo`, `autor`, `genero_id`) VALUES ('$this->titulo', '$this->autor','$this->genero')";
		
		$resultado = mysql_query($sql, $conn->conectar());
	
		return mysql_fetch_assoc($resultado);
		
	}	
	
	
	
	public function listaLivros(){
		$conn = new ConexaoModel();
		
		$sql = "SELECT livros.id,livros.titulo,livros.autor,generos.descricao 
				FROM livros AS livros
				INNER JOIN generos AS generos   
    				ON generos.id = livros.genero_id ";
		$result = mysql_query($sql, $conn->conectar());
		
		while($row = mysql_fetch_array($result)){
			$acervo[] = $row;
		}
		
		return $acervo;
	}
	
	public function getLivro($id){

		$conn = new ConexaoModel();	
	
		$sql= "SELECT id,titulo,autor,genero_id FROM `biblioteca`.`livros` WHERE `id`='$id'";
		$result = mysql_query($sql, $conn->conectar());
		$livro = mysql_fetch_assoc($result);
	
		return $livro;
	}
	public function getGenero(){
		$conn = new ConexaoModel();
		
		$query = "SELECT * FROM `biblioteca`.`generos`";
		$result = mysql_query($query, $conn->conectar());
		
		$generos = array();
		while($row = mysql_fetch_array($result)){
			$generos[] = $row;
		}
		
		return $generos;
	}
	
	public function excluir($id){
	
		$conn = new ConexaoModel();
		
		$sql = "DELETE  FROM biblioteca.livros WHERE id =$id ";
		
		$resultado = mysql_query($sql, $conn->conectar());
		
		return $resultado;
		
	}
	public function buscar($id){
		$sql	="SELECT * FROM biblioteca.livros WHERE id=$id ";
		$resultado	= mysql_query($sql,$conn->conectar());
		
		$resultadoBusca= mysql_fetch_object($resultado);
		return $resultadoBusca;
	}
	
	public function editaLivro($id){
		$conn = new ConexaoModel();
		
		$sql = "UPDATE `biblioteca`.`livros` SET `titulo`='$this->titulo', `autor`='$this->autor', `genero_id`=$this->genero WHERE `id`='$id'";
		
		
		$resultado = mysql_query($sql, $conn->conectar());
		
		return $resultado;
	
	}
	
	
	
}