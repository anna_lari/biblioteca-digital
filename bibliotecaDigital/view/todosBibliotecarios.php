<?php 
include_once '../controllers/BibliotecarioController.php';
session_start();

$obj_bibliotecarioModel = new BibliotecarioModel();

$infoBibliotecarios = $obj_bibliotecarioModel->listaBibliotecarios();

?>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Biblioteca Digital</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- Timeline CSS -->
<link href="css/plugins/timeline.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/sb-admin-2.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="css/plugins/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="font-awesome-4.1.0/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.php">Biblioteca Digital</a>
			</div>
			<!-- /.navbar-header -->

			<ul class="nav navbar-top-links navbar-right">

				<!-- /.dropdown -->

				<!-- /.dropdown -->
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#"> <i class="fa fa-user fa-fw"></i> <i
						class="fa fa-caret-down"></i>
				</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
						</li>
						<li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a></li>
						<li class="divider"></li>
						<li><a href="login.php"><i class="fa fa-sign-out fa-fw"></i>
								Logout</a></li>
					</ul> <!-- /.dropdown-user --></li>
				<!-- /.dropdown -->
			</ul>
			<!-- /.navbar-top-links -->

			<div class="navbar-default sidebar" role="navigation">
				<div class="sidebar-nav navbar-collapse">
					<ul class="nav" id="side-menu">
						<li class="sidebar-search">
							<div class="input-group custom-search-form">
								<input type="text" class="form-control"
									placeholder="Buscar Livro..."> <span class="input-group-btn">
									<button class="btn btn-default" type="button">
										<i class="fa fa-search"></i>
									</button>
								</span>
							</div> <!-- /input-group -->
						</li>
						<li><a class="active" href="index.php"><i
								class="fa fa-home fa-fw"></i> Home </a></li>
						<li><a href="#"><i class="fa fa-users fa-fw"></i> Usuários <span
								class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li><a href="todosVisitantes.php">Ver Todos Visitantes</a></li>
								<li><a href="todosBibliotecarios.php">Ver Todos Bibliotecários</a></li>
								<li><a href="novoVisitante.php">Cadastrar novo Visitante</a></li>
								<li><a href="novoBibliotecario.php">Cadastrar novo Bibliotecario</a></li>
							</ul> <!-- /.nav-second-level --></li>

						<li><a href="#"><i class="fa fa-book fa-fw"></i> Livros <span
								class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li><a href="verAcervo.php">Ver Acervo</a></li>
								<li><a href="novoLivro.php">Cadastrar novo livro</a></li>
							</ul> <!-- /.nav-second-level --></li>


					</ul>
				</div>
				<!-- /.sidebar-collapse -->
			</div>
			<!-- /.navbar-static-side -->
		</nav>

		<div id="page-wrapper">

			<!-- /.row -->
			<div class="row"></div>
			<!-- /.row -->
			<div class="col-lg-12"></div>
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Bibliotecários Cadastrados</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>
            
           
            
            <!-- /.row -->
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">Livros Existentes</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover"
									id="dataTables-example">
									<thead>
										<tr>
											<th>Nome</th>
											<th>Telefone</th>
											<th>Endereço</th>
											<th>Ações</th>
											
										</tr>
									</thead>
									<tbody>
                                    
                                    	<?php foreach ($infoBibliotecarios as $info){
												
                                    		?>
										<tr class="gradeA">
											
											<td class="gradeA"><?php echo $info['nome']?></td>
											<td class="gradeA"><?php echo $info['telefone']?></td>
											<td class="gradeA"><?php echo $info['endereco']?></td>
											<td>
												<form method="post" action="editaBibliotecario.php">
													<input type="hidden" name="acao" value="editaBibliotecario">
												 	<input type="hidden" name='id' value='<?php echo $info['id']?>'>
													<button type="submit" name="tipo"class="fa fa-edit btn btn-default" value="bibliotecario">Alterar</button>
												</form>
												<form method="post"action="../controllers/FormsController.php">
													<input type="hidden" name="acao" value="excluir"> 
													<input type="hidden" name="id" value="<?php echo $info['id']?>">
													<button type="submit" name="tipo"class="fa fa-trash-o btn btn-default" value="bibliotecario">Excluir</button>
												</form>
											</td>
										</tr>
                                        <?php } ?>
									</tbody>
								</table>


								<!-- /.panel-body -->
							</div>

							<!-- /.panel -->
						</div>
						<!-- /.col-lg-12 -->
					</div>

					<!-- /.row -->

				</div>
				<!-- /.row -->
			</div>
			<!-- /#page-wrapper -->

		</div>
		<!-- /#wrapper -->

		<!-- jQuery Version 1.11.0 -->
		<script src="js/jquery-1.11.0.js"></script>

		<!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>

		<!-- Metis Menu Plugin JavaScript -->
		<script src="js/plugins/metisMenu/metisMenu.min.js"></script>

		<!-- Morris Charts JavaScript -->
		<script src="js/plugins/morris/raphael.min.js"></script>
		<script src="js/plugins/morris/morris.min.js"></script>
		<script src="js/plugins/morris/morris-data.js"></script>

		<!-- Custom Theme JavaScript -->
		<script src="js/sb-admin-2.js"></script>

</body>

</html>
