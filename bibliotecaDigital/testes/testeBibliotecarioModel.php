<?php
require_once '../models/BibliotecarioModel.php';

class testeBibliotecarioModel extends PHPUnit_Framework_TestCase{
	protected $bibliotecarioTeste;

	public function setUp(){
		$this->bibliotecarioTeste = new BibliotecarioModel('Anna','999999','brasilia','123');
	}
	
	public function testeSalvar(){
		$return = $this->bibliotecarioTeste->salvar();
		
		$this->assertTrue($return);
	}
	
	public function testeExcluir(){
		$return = $this->bibliotecarioTeste->excluir(26);
		
		$this->assertTrue($return);
	}
	public function testeBuscar(){
		$return = $this->bibliotecarioTeste->buscar(3);
		
		$this->assertObjectHasAttribute("nome",$return);
		$this->assertObjectHasAttribute("telefone",$return);
		$this->assertObjectHasAttribute("endereco",$return);
		
	}
	
}



?>