<?php
require_once '../models/UsuarioModel.php';

class testeUsuarioModel extends PHPUnit_Framework_TestCase{
	protected $usuarioTeste;

	public function setUp(){
		$this->usuarioTeste = new UsuarioModel('Anna','999999','brasilia','1','123');
	}
	
	public function testeSalvar(){
		$return = $this->usuarioTeste->salvar();
		
		$this->assertTrue($return);
	}
	
	public function testeExcluir(){
		$return = $this->usuarioTeste->excluir(26);
		
		$this->assertTrue($return);
	}
	public function testeBuscar(){
		$return = $this->usuarioTeste->buscar(43);
		
		$this->assertObjectHasAttribute("nome",$return);
		$this->assertObjectHasAttribute("telefone",$return);
		$this->assertObjectHasAttribute("endereco",$return);
		
	}
	
}



?>