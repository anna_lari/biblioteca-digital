<?php 
ini_set ( 'display_errors', 1 );
ini_set ( 'log_errors', 1 );
ini_set ( 'error_log', dirname ( '_FILE_' ) . '/error_log.txt' );
error_reporting ( E_ALL );

include_once '../models/VisitanteModel.php';

class VisitanteController{
	
	public function salvar($nome,$telefone,$endereco,$matricula,$senha){
		$obj_visitanteModel = new VisitanteModel($nome, $telefone, $endereco, $matricula, $senha);
		return $obj_visitanteModel->salvar();
	}
	public function listaVisitantes(){
		$obj_visitanteModel = new VisitanteModel();
		return $obj_visitanteModel->listaVisitantes();
	}
	public function excluir($id){
		$obj_visitanteModel = new VisitanteModel();
		return $obj_visitanteModel->excluir($id);
	}
	public function getVisitante() {
		$obj_visitanteModel = new VisitanteModel ();
		return $obj_visitanteModel->getVisitante();
	}
	public function buscar($id){
		$obj_visitanteModel = new VisitanteModel();
		return $obj_visitanteModel->busca($id);
	} 
	public function editaVisitante($id,$nome,$telefone,$endereco,$matricula,$senha){
		$obj_visitanteModel = new VisitanteModel($id,$nome,$telefone,$endereco,$matricula,$senha);
		return $obj_visitanteModel->buscar($id);
	}
	
}
?>