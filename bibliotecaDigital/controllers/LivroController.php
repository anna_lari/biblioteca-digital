<?php
ini_set ( 'display_errors', 1 );
ini_set ( 'log_errors', 1 );
ini_set ( 'error_log', dirname ( '_FILE_' ) . '/error_log.txt' );
error_reporting ( E_ALL );

include '../models/LivroModel.php';

class LivroController{
	
	public function cadastraLivro($titulo,$autor,$genero){
		
		$obj_livroModel = new LivroModel($titulo,$autor,$genero);
		
		$obj_livroModel->cadastraLivro();
	}
	
	public function getLivro(){
		$obj_livroModel = new LivroModel();
		
		$obj_livroModel->getLivro();
	}
	
	public function getGenero($id){
		$obj_livroModel = new LivroModel();
	
		$obj_livroModel->getGenero($id);
		
	}
	public function excluir($id){
		$obj_livroModel = new LivroModel();
		
		$obj_livroModel->excluir($id);
	}
	
	public function editaLivro($id,$titulo,$autor,$genero){
		
		$obj_livroModel = new LivroModel($titulo, $autor, $genero);
		
		return $obj_livroModel->editaLivro($id);
		
	}
	
	
}
