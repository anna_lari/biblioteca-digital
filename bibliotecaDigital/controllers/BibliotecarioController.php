<?php
ini_set ( 'display_errors', 1 );
ini_set ( 'log_errors', 1 );
ini_set ( 'error_log', dirname ( '_FILE_' ) . '/error_log.txt' );
error_reporting ( E_ALL );

include '../models/BibliotecarioModel.php';


 class BibliotecarioController {
	public function salvar($nome, $telefone, $endereco, $senha) {
		$obj_bibliotecarioModel = new BibliotecarioModel ( $nome, $telefone, $endereco, $senha );
		
		$obj_bibliotecarioModel->salvar ();
	}
	public function getBibliotecario() {
		$obj_bibliotecarioModel = new BibliotecarioModel ();
		
		$obj_bibliotecarioModel->getBibliotecario ();
	}
	public function excluir($id) {
		$obj_bibliotecarioModel = new BibliotecarioModel ();
		
		$obj_bibliotecarioModel->excluir( $id );
	}
	public function editaBibliotecario($id,$nome,$telefone,$endereco,$senha){
		$obj_bibliotecarioModel = new BibliotecarioModel($nome,$telefone,$endereco,$senha);
		
		$obj_bibliotecarioModel->editaBibliotecario($id);
	}
	
	
}
