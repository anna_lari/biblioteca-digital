<?php
ini_set ( 'display_errors', 1 );
ini_set ( 'log_errors', 1 );
ini_set ( 'error_log', dirname ( '_FILE_' ) . '/error_log.txt' );
error_reporting ( E_ALL );

include_once 'LivroController.php'; 
include_once 'UsuarioController.php';
include_once 'LoginController.php';


switch ($_POST['tipo']){
	
	case 'livro':
		/**
		 *Instanciação do objeto e funções referentes a cada valor de 'tipo' (livros,usuarios, login);
		 */
	
		$obj_livroModel = new LivroModel();
		$obj_livroController= new LivroController();
		
			switch($_POST['acao']){
					
				case 'cadastrar':
							$titulo = $_POST['titulo'];
							$autor = $_POST['autor'];
							$genero= $_POST['genero'];
							
							$obj_livroController->cadastraLivro($titulo, $autor, $genero);
							
							header("Location: ../view/verAcervo.php");
							break;
		
				case  'editaLivro':
							
							$titulo = $_POST['titulo'];
							$autor = $_POST['autor'];
							$genero = $_POST['genero'];
							
							$id = $_POST['id'];
							$obj_livroController->editaLivro($id,$titulo, $autor, $genero);
							
							header("Location: ../view/verAcervo.php");
							break;
		
				case  'excluir':
							$id = $_POST['id'];
							
							$obj_livroController->excluir($id);
	
							
							header("Location: ../view/verAcervo.php");
							break;
		
				case  'pesquisar' :
							$titulo= $_POST['titulo'];
							$obj_livroController->pesquisar($titulo);
							break;
			}
			
			break;

	case 'login'     :
		
		$obj_loginModel= new LoginModel();
		$obj_loginController= new LoginController();
	
						$nome= $_POST['nome'];
						$senha = $_POST['senha'];
						
						$logado = $obj_loginModel->logar($nome);
						if($logado){
							$_SESSION['usuario'] = $logado['nome'];
							
							if($logado['perfil_id']==1){
								$_SESSION['perfil'] = "bibliotecario";
								header("Location: ../view/index_bibliotecario.php");	
							}elseif ($logado['perfil_id']==2){
								$_SESSION['perfil'] = "visitante";
								header("Location: ../view/index_visitante.php");
							}
						}
						break;
		
	
	case 'usuario'	:
		
		$obj_usuarioModel = new UsuarioModel();
		$obj_usuarioController = new UsuarioController();
		
			switch($_POST['acao']){
					
				case 'salvar':
					$nome = $_POST['nome'];
					$telefone = $_POST['telefone'];
					$endereco= $_POST['endereco'];
					$senha= $_POST['senha'];
					$perfil=$_POST['perfil'];
						
					$obj_usuarioController->salvar($nome,$telefone,$endereco,$senha,$perfil);
					
					header("Location: ../view/todosUsuarios.php");
					break;
			
				case  'editaUsuario':
					
					$nome = $_POST['nome'];
					$telefone = $_POST['telefone'];
					$endereco = $_POST['endereco'];
					$senha = $_POST['senha'];
					$perfil=$_POST['perfil'];
					
					$id = $_POST['id'];
					$obj_usuarioController->editaUsuarios($id, $nome, $telefone, $endereco, $senha,$perfil);
						
					header("Location: ../view/todosUsuarios.php");
					break;
					

			
				case  'excluir':
					$id = $_POST['id'];
						
					$obj_usuarioController->excluir($id);
			
						
					header("Location: ../view/todosUsuarios.php");
					break;
			
				case  'pesquisar' :
					$nome= $_POST['nome'];
					$obj_usuarioController->pesquisar($nome);
					break;
			}
				
			break;
					
	
}					 