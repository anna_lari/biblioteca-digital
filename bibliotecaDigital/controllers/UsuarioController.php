<?php
ini_set ( 'display_errors', 1 );
ini_set ( 'log_errors', 1 );
ini_set ( 'error_log', dirname ( '_FILE_' ) . '/error_log.txt' );
error_reporting ( E_ALL );

include_once '../models/UsuarioModel.php';


 class UsuarioController {
	public function salvar($nome, $telefone, $endereco, $senha,$perfil) {
		$obj_usuarioModel = new UsuarioModel ( $nome, $telefone, $endereco, $senha,$perfil );
		
		$obj_usuarioModel->salvar ();
	}
	public function getUsuario() {
		$obj_usuarioModel = new UsuarioModel ();
		
		$obj_usuarioModel->getUsuario ();
	}
	public function getPerfis($id){
		$obj_usuarioModel = new UsuarioModel();
	
		$obj_usuarioModel->getPerfis($id);
	}
	public function excluir($id) {
		$obj_usuarioModel = new UsuarioModel ();
		
		$obj_usuarioModel->excluir( $id );
	}
	public function editaUsuarios($id, $nome, $telefone, $endereco, $senha,$perfil){
		$obj_UsuarioModel = new UsuarioModel($nome,$telefone,$endereco,$senha,$perfil);
		
		$obj_UsuarioModel->editaUsuarios($id);
	}
	
	
	
	
}
